
pub struct Creature {
    pub type_of: CreatureType,
    pub age: u32,
    pub life_points: u8,
    pub position: (u32, u32),
    pub gene: u32,
    pub brain: u32,
    pub energy: u32
}

#[derive(PartialEq)]
pub enum CreatureType{
    Free,
    Creature,
    Stone
}

impl Creature{
    pub fn create(type_of: CreatureType, position: (u32, u32)) -> Creature {
        Creature{
            type_of,
            age: 0,
            life_points: 0,
            position,
            gene: 0,
            brain: 0,
            energy: 0
        }
    }

    pub fn mutate(self, _mutation:f32) -> Creature {       
        return self;
    }

    #[allow(dead_code)]
    pub fn inherited(self, _parent: Creature, _mutation: f32) -> Creature{
        Creature {
            type_of: CreatureType::Creature,
            age: 0,
            life_points: 0,
            position: (0,  0),
            gene: 0,
            brain: 0,
            energy: 0
        }
    }
}
