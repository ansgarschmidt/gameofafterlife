use rand::{seq::IteratorRandom, thread_rng};
use plotters::prelude::*;
use plotters::coord::types::RangedCoordf32;

use crate::creature::Creature;
use crate::creature::CreatureType;

pub struct World{
    dimension  : (u32, u32),
    spots      : Vec<Creature>,
    itterations: u32
}

impl World{
    pub fn new(dimension: (u32, u32)) -> World {
        let mut w = World {
            dimension,
            spots: Vec::new(),
            itterations: 0
        };
        
        for x in 0..w.dimension.0 {
            for y in 0..w.dimension.1{
                w.spots.push(Creature::create(CreatureType::Free, (x, y)));
            }
        }
        return w;
    }

    pub fn populate(mut self, population:u16, life_points:u8) -> World{
        let free_spots = self.get_free_spots();
        let mut rng    = thread_rng();

        println!("Free spots in the world:{}", free_spots.len());

        if population as u128> free_spots.len() as u128{
            panic!("Population does not fit into the world");
        }
    
        let sample = free_spots.iter().choose_multiple(&mut rng, population as usize);

        println!("Sample size:{}", sample.len());

        for new_pos in sample.iter() {
            for c in 0..self.spots.len() {
                if self.spots[c].position.0 == new_pos.0 && self.spots[c].position.1 == new_pos.1{
                    self.spots[c].type_of = CreatureType::Creature;
                    self.spots[c].life_points = life_points;
                }
            }
        }

        println!("Free Spots left:{}", self.get_free_spots().len());

        return self;
    }

    pub fn itterate(mut self, ammount:u16) -> World {
        for _ in 0..ammount {
            self.itterations += 1;

            for i in 0..self.spots.len() {
                // self.spots[i] = self.spots[i];//.mutate(0.1);
            }
        }
        return self;
    }

    #[allow(dead_code)]
    pub fn get_population_size(&self) -> u16 {
        let mut number = 0;

        for c in 0..self.spots.len() {
            if self.spots[c].type_of == CreatureType::Creature{
                number += 1;
            }
        }
        return number;
    }

    pub fn plot(&self) -> Result<(), Box<dyn std::error::Error>>{
        let str = format!("results/{:08}.png", self.itterations);
        let root = BitMapBackend::new(&str, (640, 480)).into_drawing_area();
        root.fill(&WHITE)?;
        let root = root.apply_coord_spec(Cartesian2d::<RangedCoordf32, RangedCoordf32>::new(
            0f32..self.dimension.0 as f32,
            0f32..self.dimension.1 as f32,
            (0..640, 0..480),
        ));

        let plot_creature = |x: u32, y: u32| {
            return EmptyElement::at((x as f32, y as f32))
                + Circle::new((0, 0), 3, ShapeStyle::from(&GREEN).filled());
        };

        let plot_stone = |x: u32, y: u32| {
            return EmptyElement::at((x as f32, y as f32))
                + Circle::new((0, 0), 3, ShapeStyle::from(&BLACK).filled());
        };

        for c in self.spots.iter(){
            if c.type_of == CreatureType::Creature {
                root.draw(&plot_creature(c.position.0, c.position.1))?;
            }
            if c.type_of == CreatureType::Stone {
                root.draw(&plot_stone(c.position.0, c.position.1))?;
            }
        }
    
        root.present()?;
        Ok(())
    }

    fn get_free_spots(&self) -> Vec<(u32, u32)>{
        let mut free_spots = Vec::new();
        
        for c in self.spots.iter(){
            if c.type_of == CreatureType::Free {
                free_spots.push(c.position);
            }
        }

        return free_spots;
    }
}
