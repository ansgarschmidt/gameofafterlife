mod creature;
mod world;

use crate::world::World;

fn main() {
    let dimension   = (120,120);
    let population  = 52;
    let life_points = 100;
    let w = World::new(dimension).populate(population, life_points);
    let w = w.itterate(15);
    w.plot().map_err(|err| println!("{:?}", err)).ok();
}
